FROM whynothugo/makepkg

# build dependencies for aur packages

RUN sudo pacman-key --init
RUN sudo pacman -Sy archlinux-keyring base-devel --noconfirm
RUN sudo pacman -Syyu --noconfirm
RUN sudo pacman -S go --noconfirm
RUN cd /tmp && git clone https://aur.archlinux.org/yay.git
RUN cd /tmp/yay && makepkg .
RUN cd /tmp/yay && sudo pacman -U *.pkg.tar.zst --noconfirm
RUN yay -Syyu --noconfirm
RUN chown -R $(whoami) ~/.gnupg/
RUN find ~/.gnupg -type f -exec chmod 600 {} \;
RUN find ~/.gnupg -type d -exec chmod 700 {} \;
RUN curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | gpg --import -

# recover keys
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 7314FBDE7D38EE5610D291B65A8ED9CFC0DB6C70
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8FD3D9A8D3800305A9FFF259D1742AD60D811D58
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BD27B07A5EF45C2ADAF70E0484818A6819AF4A9B
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 7314FBDE7D38EE5610D291B65A8ED9CFC0DB6C70
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3532E98C712BFD5C



RUN curl https://lukas-mertens.gitlab.io/fileshare/npm-6.14.11-1-any.pkg.tar.zst -o /tmp/npm.tar.zst
RUN sudo pacman -U /tmp/npm.tar.zst --noconfirm
RUN yay -Syu xorg-xauth archlinux-java-run pygtk xf86-input-wacom expac jq libthinkpad nodejs-npm2arch jameica hibiscus nvm bash hostapd iproute2 iw dnsmasq iptables util-linux procps-ng which python-mypy-protobuf imagemagick python-stringcase lib32-libstdc++5 --noconfirm
RUN sudo pacman -S libxrandr cmake tree jdk-openjdk zsh rust php go fontforge java-runtime-common libffi --noconfirm
RUN sudo sed -i'.bak' -e "s/<policy domain=\\\"delegate\\\" rights=.* pattern=\\\"gs\\\" \\/>//g" /etc/ImageMagick-7/policy.xml
#RUN sudo sed -i'.bak' -e "s/<\\/policymap>/<policy domain=\\\"coder\\\" rights=\\\"read | write\\\" pattern=\\\"PDF\\\" \\/><\\/policymap>/g" /etc/ImageMagick-7/policy.xml
#RUN sudo chmod 777 /etc/ImageMagick-7/policy.xml
RUN yay -Sc
